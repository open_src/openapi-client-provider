package com.acooly.module.openapi.client.provider.webank.message.dto;

import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * 签约订单参数
 * @author fufeng
 */
@Getter
@Setter
public class WeBankSignInfo implements Serializable {


    /**
     * 商户号
     */
    @NotBlank
    protected String merId;

    /**
     * 交易订单号
     */
    protected String orderId;

    /**
     * 交易类型，固定值09
     */
    protected String bizType = "09";

    /**
     * 请求日期 YYYYMMDD
     */
    protected String reqDate = new SimpleDateFormat("yyyyMMdd").format(new Date());

    /**
     * 请求时间 HHmmSS
     */
    protected String reqTime = new SimpleDateFormat("HHmmss").format(new Date());


    /**
     * 账号
     */
    protected String acctNo;

    /**
     * 户名
     */
    protected String acctName;

    /**
     * 手机号
     */
    protected String phoneNo;

    /**
     * 证件类型
     */
    protected String certType = "01";

    /**
     * 证件号
     */
    protected String certId;

    /**
     * 备注
     */
    protected String remark;

    /**
     * 短信码
     */
    protected String smsCode;

    /**
     * 短信订单号
     */
    protected String smsOrderId;


}
