/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-25 14:40 创建
 */
package com.acooly.module.openapi.client.provider.yipay;

import com.acooly.module.openapi.client.provider.yipay.domain.YipayNotify;
import com.acooly.module.openapi.client.provider.yipay.message.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author zhangpu 2017-09-25 14:40
 */
@Component
public class YipayApiService {

    @Resource(name = "yipayApiServiceClient")
    private YipayApiServiceClient yipayApiServiceClient;


    /**
     * 全网运营商三要素验证
     * @param request
     * @return
     */
    public YipayOperatorThreeElmsValidResponse operatorThreeElmsValid(YipayOperatorThreeElmsValidRequest request) {
        return (YipayOperatorThreeElmsValidResponse)yipayApiServiceClient.execute(request);
    }

    /**
     * 銀行卡三要素验证
     * @param request
     * @return
     */
    public YipayThreeElementsVerifyResponse threeElementsVerify(YipayThreeElementsVerifyRequest request) {
        return (YipayThreeElementsVerifyResponse)yipayApiServiceClient.execute(request);
    }

    /**
     * 銀行卡四要素验证
     * @param request
     * @return
     */
    public YipayFourElementsVerifyResponse fourElementsVerify(YipayFourElementsVerifyRequest request) {
        return (YipayFourElementsVerifyResponse)yipayApiServiceClient.execute(request);
    }

    /**
     * 全网实时位置验证接口
     * @param request
     * @return
     */
    public YipayRealTimeLocationVerifyPositionResponse realTimeLocationVerifyPosition(YipayRealTimeLocationVerifyPositionRequest request) {
        return (YipayRealTimeLocationVerifyPositionResponse)yipayApiServiceClient.execute(request);
    }

    /**
     * 营商实时城市位置核验接口
     * @param request
     * @return
     */
    public YipayCityLocationVerifyLocationResponse cityLocationVerifyLocation(YipayCityLocationVerifyLocationRequest request) {
        return (YipayCityLocationVerifyLocationResponse)yipayApiServiceClient.execute(request);
    }

    /**
     * 运营商在网状态查询
     * @param request
     * @return
     */
    public YipayPhoneInfoValidOnlineStatusResponse phoneInfoValidOnlineStatus(YipayPhoneInfoValidOnlineStatusRequest request) {
        return (YipayPhoneInfoValidOnlineStatusResponse)yipayApiServiceClient.execute(request);
    }

    /**
     * 运营商入网时长查询
     * @param request
     * @return
     */
    public YipayWholeNetworkMobileTimeQueryResponse wholeNetworkMobileTimeQuery(YipayWholeNetworkMobileTimeQueryRequest request) {
        return (YipayWholeNetworkMobileTimeQueryResponse)yipayApiServiceClient.execute(request);
    }

    /**
     * 电信运营商二要素验证(手机号+身份证)
     * @param request
     * @return
     */
    public YipayTwoElemValidByIdResponse twoElemValidById(YipayTwoElemValidByIdRequest request) {
        return (YipayTwoElemValidByIdResponse)yipayApiServiceClient.execute(request);
    }

    /**
     * 电信运营商二要素验证(手机号+姓名)
     * @param request
     * @return
     */
    public YipayTwoElemValidByNameResponse twoElemValidByName(YipayTwoElemValidByNameRequest request) {
        return (YipayTwoElemValidByNameResponse)yipayApiServiceClient.execute(request);
    }

    /**
     * 身份证二要素验证接口
     * @param request
     * @return
     */
    public YipayCertTwoElementsVerifyResponse certTwoElementsVerify(YipayCertTwoElementsVerifyRequest request) {
        return (YipayCertTwoElementsVerifyResponse)yipayApiServiceClient.execute(request);
    }

    /**
     * 异步实时代收
     * @param request
     * @return
     */
    public YipayDeductResponse deduct(YipayDeductRequest request) {
        return (YipayDeductResponse)yipayApiServiceClient.execute(request);
    }

    /**
     * 交易查询
     * @param request
     * @return
     */
    public YipayTradeQueryResponse tradeQuery(YipayTradeQueryRequest request) {
        return (YipayTradeQueryResponse)yipayApiServiceClient.execute(request);
    }

    /**
     * 同步，异步通知转化
     * serviceKey :YipayServiceNameEnum枚举中取对应的code
     * @param request 请求
     * @param serviceKey 服务标识
     * @return
     */
    public YipayNotify notice(HttpServletRequest request, String serviceKey) {
        return  yipayApiServiceClient.notice(request, serviceKey);
    }
}
