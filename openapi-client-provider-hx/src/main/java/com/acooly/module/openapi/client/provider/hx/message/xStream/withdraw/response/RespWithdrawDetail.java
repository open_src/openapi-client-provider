package com.acooly.module.openapi.client.provider.hx.message.xStream.withdraw.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

/**
 * @author fufeng 2018/2/28 19:43.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("Detail")
public class RespWithdrawDetail {

    /**
     *商户订单号
     */
    @XStreamAlias("MerBillno")
    private String merBillno;
    /**
     *错误编码
     */
    @XStreamAlias("ErrorCode")
    private String errorCode;
    /**
     *错误信息
     */
    @XStreamAlias("ErrorMsg")
    private String errorMsg;

}
