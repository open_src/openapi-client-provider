/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月1日
 *
 */
package com.acooly.module.openapi.client.provider.hx.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.hx.domain.HxRequest;

import org.springframework.stereotype.Service;

/**
 * @author fufeng
 */
@Service
public class HxRequestMarshall extends HxMarshallSupport implements ApiMarshal<String, HxRequest> {

    @Override
    public String marshal(HxRequest source) {
        beforeMarshall(source);
        return doMarshall(source);
    }

}
