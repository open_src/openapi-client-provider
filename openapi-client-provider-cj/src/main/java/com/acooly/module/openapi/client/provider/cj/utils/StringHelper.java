package com.acooly.module.openapi.client.provider.cj.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StringHelper {

    /**
     * 将byte[] 转换成字符串
     *
     * @return
     */
    public static String byte2Hex(byte[] srcBytes) {
        StringBuilder hexRetSB = new StringBuilder();
        for (byte b : srcBytes) {
            String hexString = Integer.toHexString(0x00ff & b);
            hexRetSB.append(hexString.length() == 1 ? 0 : "").append(hexString);
        }
        return hexRetSB.toString();
    }

    public static String getCurrentTimestamp() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        Calendar calendar = Calendar.getInstance();
        String createTime = format.format(calendar.getTime());
        return createTime;
    }

    /**
     * 将16进制字符串转为转换成字符串
     *
     * @param source
     * @return
     */
    public static byte[] hex2Bytes(String source) {
        byte[] sourceBytes = new byte[source.length() / 2];
        for (int i = 0; i < sourceBytes.length; i++) {
            sourceBytes[i] = (byte) Integer.parseInt(source.substring(i * 2, i * 2 + 2), 16);
        }
        return sourceBytes;
    }

    public static String nvl(String str) {
        if (StringUtils.isBlank(str)) {
            return "";
        }
        return str.trim();
    }//method

    public static String nvl(Long val) {
        return val == null ? "" : val + "";
    }//method

    public static String nvl(Integer val) {
        return val == null ? "" : val + "";
    }//method

    /**
     * 生成单行类描述字串
     */
    public static String build2StringShortStyle(Object obj) {
        String ts = null;
        ts = ToStringBuilder.reflectionToString(obj, ToStringStyle.SHORT_PREFIX_STYLE);
        return ts;
    }


    /**
     * 安照GBK编码截取计算字符串实际的字节长度，主要用于存储如数据库时，限定字符长度使用。<br>
     * varchar2(2000)的字段，只能存储1000个中文字符
     * @param src 原始字符串
     * @param len 指定的长度
     * @return
     */
    public static String substringByByte(String src, final int len) {
        if (src.length() < (len / 2))
            return src;

        try {
            byte[] bs = src.getBytes("GBK");

            //字符长度小于给定长度
            if (bs.length <= len) {
                return src;
            }

            //如果没有双字节字
            if (bs.length == src.length()) {
                return src.substring(0, len);
            }

            //处理双字节情况
            StringBuilder sb = new StringBuilder();
            int size = 0;
            int cnt = 0;
            for (Character ch : src.toCharArray()) {
                cnt = Character.toString(ch).getBytes("GBK").length;
                size += cnt;
                if (size <= len) {
                    sb.append(ch);
                }
            }
            return sb.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }//method

}
