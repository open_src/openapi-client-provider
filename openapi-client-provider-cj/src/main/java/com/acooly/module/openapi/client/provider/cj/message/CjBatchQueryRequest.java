package com.acooly.module.openapi.client.provider.cj.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.cj.domain.CjApiMsgInfo;
import com.acooly.module.openapi.client.provider.cj.domain.CjRequest;
import com.acooly.module.openapi.client.provider.cj.enums.CjServiceEnum;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@CjApiMsgInfo(service = CjServiceEnum.CJ_BATCH_DEDUCT_QUERY, type = ApiMessageType.Request)
public class CjBatchQueryRequest extends CjRequest {

    /** 原交易请求号 */
    private String qryReqSn;
    /** 原交易明细号 */
    private String sn;

    /** 订单状态 */
    private String status;
    /** 订单信息描述 */
    private String detail;

}
