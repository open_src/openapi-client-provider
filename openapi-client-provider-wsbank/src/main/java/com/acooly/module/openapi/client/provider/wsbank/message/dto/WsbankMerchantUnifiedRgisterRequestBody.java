package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankAccountTypeEnum;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankMerchantTypeEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankCommonMerchantClearCard;
import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankCommonMerchantDetail;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author sunjx 2018/5/23 14:00
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankMerchantUnifiedRgisterRequestBody implements Serializable {

	private static final long serialVersionUID = -4957566653955247068L;

	/**
	 * 合作方机构号
	 */
	@Size(max = 32)
	@XStreamAlias("IsvOrgId")
	@NotBlank
	private String isvOrgId;

	/**
	 * 外部商户号
	 */
	@Size(max = 32)
	@XStreamAlias("OutMerchantId")
	@NotBlank
	private String outMerchantId;

	/**
	 * 商户名称
	 */
	@Size(max = 50)
	@XStreamAlias("MerchantName")
	@NotBlank
	private String merchantName;

	/**
	 * 商户类型
	 */
	@Size(max = 8)
	@XStreamAlias("MerchantType")
	@NotBlank
	private String merchantType;

	/**
	 * 商户经营类型
	 */
	@Size(max = 8)
	@XStreamAlias("DealType")
	@NotBlank
	private String dealType = "01";

	/**
	 * 商户清算资金是否支持T+0到账
	 */
	@Size(max = 8)
	@XStreamAlias("SupportPrepayment")
	@NotBlank
	private String supportPrepayment = "N";

	/**
	 * 负责人证件类型
	 */
	@Size(max = 8)
	@XStreamAlias("PrincipalCertType")
	@NotBlank
	private String principalCertType = "01";
	
	/**
	 * 结算方式
	 */
	@Size(max = 8)
	@XStreamAlias("SettleMode")
	@NotBlank
	private String settleMode = "05";

	/**
	 * 线下经营类目
	 */
	@Size(max = 16)
	@XStreamAlias("Mcc")
	@NotBlank
	private String mcc = "2016062900190337";

	/**
	 * 支付宝线上经营类目
	 */
	@Size(max = 4)
	@XStreamAlias("OnlineMcc")
	@NotBlank
	private String onlineMcc = "5411";

	/**
	 * 商户详情
	 */
	@XStreamOmitField
	@NotNull
	private WsbankCommonMerchantDetail wsbankCommonMerchantDetail;
	
	/**
	 * 商户详情列表
	 */
	@XStreamAlias("MerchantDetail")
	@NotBlank
	private String merchantDetail;
	
	/**
	 * 支付宝线上站点信息
	 */
	@XStreamAlias("SiteInfo")
	@NotBlank
	private String siteInfo;

	/**
	 * 支持交易类型列表
	 */
	@XStreamAlias("TradeTypeList")
	@NotBlank
	private String tradeTypeList = "01,02,06,08,09";

	/**
	 * 支持支付渠道列表
	 */
	@XStreamAlias("PayChannelList")
	@NotBlank
	private String payChannelList = "01,02,05";

	/**
	 * 禁用支付方式
	 */
	@XStreamAlias("DeniedPayToolList")
	private String deniedPayToolList;
	
	/**
	 * 手续费列表
	 */
	@XStreamAlias("FeeParamList")
	@NotBlank
	private String feeParamList;

	/**
	 * 清算卡
	 */
	@XStreamOmitField
	@NotNull
	private WsbankCommonMerchantClearCard wsbankCommonMerchantClearCard;
	
	/**
	 * 清算卡参数
	 */
	@XStreamAlias("BankCardParam")
	private String bankCardParam;

	/**
	 * 手机验证码
	 */
	@Size(max = 32)
	@XStreamAlias("AuthCode")
	private String authCode;

	/**
	 * 外部交易号
	 */
	@Size(max = 64)
	@XStreamAlias("OutTradeNo")
	@NotBlank
	private String outTradeNo;

	/**
	 * 该字段用于决定商户是否可自主选择让其买家使用花呗分期，包括商户承担手续费和用户承担手续费两种模式
	 */
	@Size(max = 1)
	@XStreamAlias("SupportStage")
	private String supportStage = "1";

	/**
	 * 商户在进行微信支付H5支付时所使用的公众号相关信息的类型
	 */
	@Size(max = 2)
	@XStreamAlias("PartnerType")
	@NotBlank
	private String partnerType = "03";

	/**
	 * 用于记录商户由哪一个渠道接入支付宝的，该字段用于营销激励、风控等方面。是谁带来的商户就填写谁的PID
	 */
	@Size(max = 32)
	@XStreamAlias("AlipaySource")
	private String alipaySource = "2088021604845422";

	/**
	 * ISV可线下联系网商运营申请支持指定微信渠道号入驻，该字段用于指定在哪个微信渠道号下进件
	 */
	@Size(max = 10)
	@XStreamAlias("WechatChannel")
	private String WechatChannel;

	/**
	 * 费率版本字段
	 */
	@Size(max = 16)
	@XStreamAlias("RateVersion")
	private String RateVersion;

	/**
	 * 验证
	 */
	public void doCheck() {
		//验证商户详情
		Validators.assertJSR303(this.wsbankCommonMerchantDetail);
		//验证清算卡信息
		Validators.assertJSR303(this.wsbankCommonMerchantClearCard);
		//验证个体工商户
		if(WsbankMerchantTypeEnum.SELF.code().equals(this.merchantType)){
			//短信验证码不能为空
			//if(StringUtils.isBlank(this.getAuthCode())){
			//	throw new ApiClientException("短信验证码不能为空!");
			//}
			//验证账户类型
			if(!WsbankAccountTypeEnum.PRIVATE_ACCOUNT.code().equals(wsbankCommonMerchantClearCard.getAccountType())){
				throw new ApiClientException("个体工商户账户类型必须为对私账户!");
			}
		}
		//验证企业商户
		if(WsbankMerchantTypeEnum.INDUSTRY.code().equals(this.merchantType)){
			//验证开户许可证照片
			if(StringUtils.isBlank(wsbankCommonMerchantDetail.getIndustryLicensePhoto())){
				throw new ApiClientException("企业商户开户许可证照片不能为空!");
			}
			//验证企业法人名称
			if(StringUtils.isBlank(wsbankCommonMerchantDetail.getLegalPerson())){
				throw new ApiClientException("企业法人名称不能为空!");
			}
			//验证账户类型
			if(!WsbankAccountTypeEnum.PUBLIC_ACCOUNT.code().equals(wsbankCommonMerchantClearCard.getAccountType())){
				throw new ApiClientException("企业商户账户类型必须为对公账户!");
			}
			//联行号不能为空
			if(StringUtils.isBlank(wsbankCommonMerchantClearCard.getContactLine())){
				throw new ApiClientException("联行号不能为空!");
			}
		}
	}
}
