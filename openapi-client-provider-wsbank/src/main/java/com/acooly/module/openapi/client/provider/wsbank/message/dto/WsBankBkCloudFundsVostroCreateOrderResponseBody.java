package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("body")
public class WsBankBkCloudFundsVostroCreateOrderResponseBody implements Serializable {

	private static final long serialVersionUID = 5999916245778749722L;

	/**
     * 返回码组件。当ResultStatus=S时才有后续的参数返回。
     */
    @XStreamAlias("RespInfo")
    private WsbankResponseInfo responseInfo;
    
	/**
	 * 外部交易号
	 */
	@XStreamAlias("OutTradeNo")
	private String outTradeNo;
	
	/**
	 * 网商支付订单号
	 */
	@XStreamAlias("OrderNo")
	private String orderNo;
	
	/**
	 * 收款方卡号
	 */
	@XStreamAlias("PayeeCardNo")
	private String payeeCardNo;
	
	/**
	 * 收款方户名
	 */
	@XStreamAlias("PayeeName")
	private String payeeName;
}
