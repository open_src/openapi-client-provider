package com.acooly.module.openapi.client.provider.wsbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankNotify;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankServiceEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankOnlinePayNoticeNotifyInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/5/24 14:03
 */
@Getter
@Setter
@XStreamAlias("document")
@WsbankApiMsgInfo(service = WsbankServiceEnum.ONLINE_PAY_NOTICE,type = ApiMessageType.Notify)
public class WsbankOnlinePayNoticeNotify extends WsbankNotify {

    /**
     * 支付宝APP/WAP支付结果通知
     */
    @XStreamAlias("request")
    private WsbankOnlinePayNoticeNotifyInfo requestInfo;
}
