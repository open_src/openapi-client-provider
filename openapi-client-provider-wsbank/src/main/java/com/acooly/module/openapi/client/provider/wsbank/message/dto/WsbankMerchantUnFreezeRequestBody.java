package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author weili 2018/5/22 16:00
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankMerchantUnFreezeRequestBody implements Serializable {

    /**
     *合作方机构号（网商银行分配）
     */
    @Size(max = 32)
    @XStreamAlias("IsvOrgId")
    @NotBlank
    private String isvOrgId;

    @Size(max = 32)
    @XStreamAlias("OutTradeNo")
    @NotBlank
    private String outTradeNo;


    @Size(max = 64)
    @XStreamAlias("MerchantId")
    @NotBlank
    private String merchantId;
    
    @Size(max = 256)
    @XStreamAlias("UnfreezeReason")
    @NotBlank
    private String unfreezeReason;

  




}


