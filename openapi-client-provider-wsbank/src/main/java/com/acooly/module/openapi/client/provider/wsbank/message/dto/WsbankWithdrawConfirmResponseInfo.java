package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankHeadResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author sunjx
 */
@Getter
@Setter
@XStreamAlias("response")
public class WsbankWithdrawConfirmResponseInfo implements Serializable {

	private static final long serialVersionUID = -1321741075211305313L;

	/**
     *响应报文头
     */
    @XStreamAlias("head")
    private WsbankHeadResponse headResponse;

    /**
     * 响应报文体
     */
    @XStreamAlias("body")
    private WsbankWithdrawConfirmResponseBody responseBody;
}
