package com.acooly.openapi.client.provider.yuejb;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.core.utils.Money;
import com.acooly.module.openapi.client.provider.yuejb.YueJBApiService;
import com.acooly.module.openapi.client.provider.yuejb.message.YueJBFreezeRequest;
import com.acooly.module.openapi.client.provider.yuejb.message.YueJBLoanRequest;
import com.acooly.test.NoWebTestBase;
import com.google.common.collect.Maps;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Map;

/**
 * Created by ouwen@yiji.com} on 2017/11/7.
 */
@SpringBootApplication
@BootApp(sysName = "YueJBTest")
public class YueJBTest extends NoWebTestBase {
    @Autowired
    private YueJBApiService yueJBApiService;

    @Test
    public void TestFreeze() {
        YueJBFreezeRequest yueJBFreezeRequest = new YueJBFreezeRequest();
        yueJBFreezeRequest.setMemo("测试");
        yueJBFreezeRequest.setAmount(Money.cent(10l));
        yueJBFreezeRequest.setProductNo("201701102");
        yueJBFreezeRequest.setStoreId("20140514020000003834");
        yueJBFreezeRequest.setMarkCode("20171102000000002934");
        yueJBFreezeRequest.setMerchOrderNo(Ids.oid());
        yueJBApiService.freeze(yueJBFreezeRequest);
    }

    @Test
    public void TestLoad() {
        YueJBLoanRequest yueJBLoanRequest = new YueJBLoanRequest();
        yueJBLoanRequest.setCreditAmount(Money.cent(1));
        yueJBLoanRequest.setFreezeQuotaBizId("006x01v01hjr6ap69k00");
        yueJBLoanRequest.setLoanAmount(Money.cent(1));
        yueJBLoanRequest.setMarkCode("20171102000000002934");
        yueJBLoanRequest.setProductNo("201701102");
        yueJBLoanRequest.setReceiptAccountClass("C");
        yueJBLoanRequest.setReceiptAccountName("欧文");
        yueJBLoanRequest.setReceiptAccountNo("6217003760015345557");
        yueJBLoanRequest.setReceiptAccountType("BANK");
        yueJBLoanRequest.setReceiptBankCode("ICBC");
        yueJBLoanRequest.setUnfreezeAll("true");
        yueJBLoanRequest.setMerchOrderNo(Ids.oid());
        yueJBApiService.loan(yueJBLoanRequest);
    }
    @Test
    public void TestNotify(){
        Map<String,String> datas = Maps.newHashMap();
        datas.put("loanNo","DK201711150010000423");
        datas.put("service","sellOnCreditLend");
        datas.put("notifyTime","2017-11-15 09:39:09");
        datas.put("sign","afb0ae1b5f32ea4f949589e3c4fe87b5");
        datas.put("description","");
        datas.put("merchOrderNo","test17111509193342600001");
        datas.put("signType","MD5");
        datas.put("partnerId","20171102000000002933");
        datas.put("message","成功");
        datas.put("version","1.0");
        datas.put("status","SUCCESS");
        yueJBApiService.notifyMessage(datas);
    }
}
