package com.acooly.openapi.client.provider.fudian;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.fudian.FudianApiService;
import com.acooly.module.openapi.client.provider.fudian.OpenAPIClientFudianProperties;
import com.acooly.module.openapi.client.provider.fudian.message.fund.*;
import com.acooly.module.openapi.client.provider.fudian.message.fund.dto.LoanCallbackInvestInfo;
import com.acooly.module.openapi.client.provider.fudian.message.member.*;
import com.acooly.module.openapi.client.provider.fudian.message.other.MigrationDataRequest;
import com.acooly.module.openapi.client.provider.fudian.message.other.MigrationDataResponse;
import com.acooly.module.openapi.client.provider.fudian.message.query.*;
import com.alibaba.fastjson.JSON;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

/**
 * @author zhangpu@yiji.com
 */
@SpringBootApplication
@BootApp(sysName = "test")
public class FudianTest extends FudianAbstractTest {

    @Autowired
    private FudianApiService fudianApiService;

    @Autowired
    private OpenAPIClientFudianProperties openAPIClientFudianProperties;

    /**
     * 测试用户注册
     */
    @Test
    public void testUserRegister(){
        UserRegisterRequest request = new UserRegisterRequest();
        request.setMobilePhone("13206167038");
        request.setRealName("代崇凯");
        request.setIdentityType("1");
        request.setRoleType("1");
        request.setIdentityCode("421223198511294858");
        request.setReturnUrl("http://218.70.106.250:9090/fudian/notify/corp/userRegisterReturn");
        String response = fudianApiService.userRegister(request);
        System.out.println("用户注册响应报文："+response);

    }

    /**
     * 测试用户绑卡
     */
    @Test
    public void testUserCardBind(){
        UserCardBindRequest request = new UserCardBindRequest();
        request.setAccountNo("UA02580551305381001");
        request.setUserName("UU02580551299751001");
        request.setReturnUrl("http://218.70.106.250:9090/fudian/notify/corp/userCardBindReturn");
        String response = fudianApiService.userCardBind(request);
        System.out.println("用户绑卡响应报文："+response);

    }

    /**
     * 测试用户解绑银行卡(不需要审核)
     */
    @Test
    public void testUserCardCancelBind(){
        UserCardCancelBindRequest request = new UserCardCancelBindRequest();
        request.setAccountNo("UA02579819520851001");
        request.setUserName("UU02579819517821001");
        request.setReturnUrl("http://218.70.106.250:9090/fudian/notify/corp/userCardCancelBindReturn");
        String response = fudianApiService.userCardCancelBind(request);
        System.out.println("用户解绑卡(不需要审核)响应报文："+response);

    }

    /**
     * 测试用户解绑银行卡（需要审核）
     */
    @Test
    public void testUserAuditCardCancelBind(){
        UserCardAuditCancelBindRequest request = new UserCardAuditCancelBindRequest();
        request.setAccountNo("UA02579819520851001");
        request.setUserName("UU02579819517821001");
        request.setReturnUrl("http://218.70.106.250:9090/fudian/notify/corp/userCardCancelBindReturn");
        String response = fudianApiService.userCardAutitCancelBind(request);
        System.out.println("用户解绑卡（需要审核）响应报文："+response);

    }

    /**
     * 测试用户修改手机号码
     */
    @Test
    public void testPhoneUpdate(){
        PhoneUpdateRequest request = new PhoneUpdateRequest();
        request.setAccountNo("UA02579819520851001");
        request.setUserName("UU02579819517821001");
        request.setNewPhone("18696725228");
        request.setType("1");
        request.setReturnUrl("http://218.70.106.250:9090/fudian/notify/corp/phoneUpdateReturn");
        String response = fudianApiService.phoneUpdate(request);
        System.out.println("用户绑卡响应报文："+response);

    }

    /**
     * 测试重置交易密码
     */
    @Test
    public void testPwdReset(){
        PwdResetRequest request = new PwdResetRequest();
        request.setAccountNo("CA02580267243041001");
        request.setUserName("CU02580265970931001");
        request.setReturnUrl("http://218.70.106.250:9090/fudian/notify/corp/pwdResetReturn");
        String response = fudianApiService.pwdReset(request);
        System.out.println("用户绑卡响应报文："+response);

    }

    /**
     * 测试授权与取消授权
     */
    @Test
    public void testBusinessAuthorization(){
        BusinessAuthorizationRequest request = new BusinessAuthorizationRequest();
        request.setAccountNo("UA02580551305381001");
        request.setUserName("UU02580551299751001");
        request.setBusinessType("1");
        request.setReturnUrl("http://218.70.106.250:9090/fudian/notify/corp/businessAuthorizationReturn");
        String response = fudianApiService.businessAuthorization(request);
        System.out.println("授权与取消授权响应报文："+response);
    }

    /**
     * 测试企业开户
     */
    @Test
    public void testCorpRegister(){
        CorpRegisterRequest request = new CorpRegisterRequest();
        request.setReturnUrl("https://www.baidu.com");
        String response = fudianApiService.corpRegister(request);
        System.out.println(response);

    }

    /**
     * 测试企业信息变更
     */
    @Test
    public void testCorpModify(){
        CorpModifyRequest request = new CorpModifyRequest();
        request.setArtificialIdentityCode("610528199303200025");
        request.setUserName("CU02580265970931001");
        request.setArtificialRealName("互金");
        request.setCorpName("北京金财富金融刘局有限公司");
        request.setModifyType("1");
        request.setMobilePhone("13867874822");
        request.setOldCorpName("北京金财富中刘局有限公司");
        request.setReturnUrl("http://218.70.106.250:9090/fudian/notify/corp/corpModifyReturn");
        String response = fudianApiService.corpModify(request);
        System.out.println("授权与取消授权响应报文："+response);
    }

    /**
    * 测试充值申请接口
     */
    @Test
    public void testAccountRecharge() {
        AccountRechargeRequest request = new AccountRechargeRequest();
        request.setAccountNo("UA02580551305381001");
        request.setAmount("100000");
        request.setFee("0.2");
        request.setPayType("1");
        request.setUserName("UU02580551299751001");
        request.setReturnUrl("http://218.70.106.250:9090/fudian/notify/corp/accountRechargeReturn");
        String response = fudianApiService.accountRecharge(request);
        System.out.println("PC跳转充值响应报文："+response);
    }

    /**
     * 测试APP充值申请接口
     */
    @Test
    public void testAppAccountRecharge() {
        AppRealPayRechargeRequest request = new AppRealPayRechargeRequest();
        request.setAccountNo("UA02580551305381001");
        request.setAmount("100000");
        request.setFee("0.2");
        request.setPayType("1");
        request.setUserName("UU02580551299751001");
        request.setReturnUrl("http://218.70.106.250:9090/fudian/notify/corp/accountRechargeReturn");
        String response = fudianApiService.appRealPayRecharge(request);
        System.out.println("APP跳转充值响应报文："+response);
    }


    /**
     * 测试提现申请接口
     */
    @Test
    public void testAccountWithdraw() {
        AccountWithdrawRequest request = new AccountWithdrawRequest();
        request.setAccountNo("UA02580551305381001");
        request.setAmount("20");
        request.setFee("0.2");
        request.setUserName("UU02580551299751001");
        request.setReturnUrl("http://218.70.106.250:9090/fudian/notify/corp/accountWithdrawReturn");
        String response = fudianApiService.accountWithdraw(request);
        System.out.println("PC跳转充值响应报文："+response);
    }

    /**
     * 测试发标接口
     */
    @Test
    public void testLoanCreate() {
        LoanCreateRequest request = new LoanCreateRequest();
        request.setAccountNo("UA02579819520851001");
        request.setAmount("10000");
        request.setLoanName("测试标的1");
        request.setLoanType("1");
        request.setUserName("UU02579819517821001");
        LoanCreateResponse response = fudianApiService.loanCreate(request);
        System.out.println("发标响应报文："+response);
    }

    /**
     * 测试撤标（流标）接口
     */
    @Test
    public void testLoanCancel() {
        LoanCancelRequest request = new LoanCancelRequest();
        request.setLoanOrderDate("20180306");
        request.setLoanOrderNo("18030616015260370001");
        request.setLoanTxNo("LU02580480979691001");
        LoanCancelResponse response = fudianApiService.loanCancel(request);
        System.out.println("撤标（流标）响应报文："+response);
    }

    /**
     * 测试手动投标接口
     */
    @Test
    public void testLoanInvest() {
        LoanInvestRequest request = new LoanInvestRequest();
        request.setAccountNo("UA02580551305381001");
        request.setAmount("9896.8");
        request.setAward("0");
        request.setUserName("UU02580551299751001");
        request.setLoanTxNo("LU02580533285161001");
        request.setReturnUrl("http://218.70.106.250:9090/fudian/notify/corp/loanInvestReturn");
        String response = fudianApiService.loanInvest(request);
        System.out.println("手动投标响应报文："+response);
    }

    /**
     * 先要对此用户进行授权
     * 测试自动投标接口
     */
    @Test
    public void testLoanFastInvest() {
        LoanFastInvestRequest request = new LoanFastInvestRequest();
        request.setAccountNo("UA02580551305381001");
        request.setUserName("UU02580551299751001");
        request.setAmount("3.2");
        request.setAward("0");
        request.setLoanTxNo("LU02580533285161001");
        LoanFastInvestResponse response = fudianApiService.loanFastInvest(request);
        System.out.println("自动投标响应报文："+response);
    }



    /**
     * 测试满标放款接口
     */
    @Test
    public void testLoanFull() {
        LoanFullRequest request = new LoanFullRequest();
        request.setAccountNo("UA02579819520851001");
        request.setUserName("UU02579819517821001");
        request.setLoanTxNo("LU02580533285161001");
        request.setLoanFee("100");
        request.setLoanOrderDate("20180306");
        request.setLoanOrderNo("18030617290300600001");
        request.setExpectRepayTime("20180307");
        LoanFullResponse response = fudianApiService.loanFull(request);
        System.out.println("满标放款响应报文："+response);
    }

    /**
     * 测试手动还款接口
     */
    @Test
    public void testLoanRepay() {
        LoanRepayRequest request = new LoanRepayRequest();
        request.setAccountNo("UA02579819520851001");
        request.setCapital("100");
        request.setInterest("10");
        request.setUserName("UU02579819517821001");
        request.setLoanTxNo("LU02580533285161001");
        request.setLoanFee("10");
        request.setReturnUrl("http://218.70.106.250:9090/fudian/notify/corp/loanRepayReturn");
        String response = fudianApiService.loanRepay(request);
        System.out.println("手动还款响应报文："+response);
    }

    /**
     * 测试自动免密还款接口
     */
    @Test
    public void testLoanFastRepay() {
        LoanFastRepayRequest request = new LoanFastRepayRequest();
        request.setAccountNo("UA02579819520851001");
        request.setCapital("100");
        request.setInterest("10");
        request.setUserName("UU02579819517821001");
        request.setLoanTxNo("LU02580533285161001");
        request.setLoanFee("10");
        LoanFastRepayResponse response = fudianApiService.loanFastRepay(request);
        System.out.println("自动免密还款响应报文："+response);
    }

    /**
     * 测试债权认购接口
     */
    @Test
    public void testLoancreditInvest() {
        LoancreditInvestRequest request = new LoancreditInvestRequest();
        request.setCreditNo(Ids.oid());
        request.setAmount("100");
        request.setCreditNoAmount("100");
        request.setCreditAmount("100");
        request.setAccountNo("100");
        request.setCreditFee("0");
        request.setCreditFeeType("1");
        request.setInvestOrderDate("20180306");
        request.setInvestOrderNo("18030617290300600001");
        request.setOriOrderDate("20180306");
        request.setOriOrderNo("18030617290300600001");
        request.setRepayedAmount("3");
        request.setAccountNo("UA02579819520851001");
        request.setUserName("UU02579819517821001");
        request.setLoanTxNo("LU02580533285161001");
        request.setReturnUrl("http://218.70.106.250:9090/fudian/notify/corp/loancreditInvestReturn");
        String response = fudianApiService.loancreditInvest(request);
        System.out.println("债权认购响应报文："+response);
    }


    /**
     * 测试商户转账（奖励）接口
     */
    @Test
    public void testMerchantTransfer() {
        MerchantTransferRequest request = new MerchantTransferRequest();
        request.setAccountNo("UA02580551305381001");
        request.setUserName("UU02580551299751001");
        request.setAmount("3.2");
        MerchantTransferResponse response = fudianApiService.merchantTransfer(request);
        System.out.println("自动投标响应报文："+response);
    }

    /**
     * 测试订单查询接口
     */
    @Test
    public void testQueryTrade() {
        QueryTradeRequest request = new QueryTradeRequest();
        request.setQueryOrderNo("18030621041040700001");
        request.setQueryType("04");
        request.setQueryOrderDate("20180306");
        QueryTradeResponse response = fudianApiService.queryTrade(request);
        System.out.println("订单查询响应报文："+response);
    }

    /**
     * 测试订单查询接口
     */
    @Test
    public void testQueryUser() {
        QueryUserRequest request = new QueryUserRequest();
        request.setAccountNo("UA02579819520851001");
        request.setUserName("UU02579819517821001");
        QueryUserResponse response = fudianApiService.queryUser(request);
        System.out.println("用户信息查询响应报文："+response);
    }

    /**
     * 测试标的账户查询接口
     */
    @Test
    public void testQueryLoan() {
        QueryLoanRequest request = new QueryLoanRequest();
        request.setLoanTxNo("LU02580533285161001");
        QueryLoanResponse response = fudianApiService.queryLoan(request);
        System.out.println("标的账户查询响应报文："+response);
    }

    /**
     * 测试标的流水查询接口
     */
    @Test
    public void testQueryLogLoanAccount() {
        QueryLogLoanAccountRequest request = new QueryLogLoanAccountRequest();
        request.setLoanTxNo("LU02580533285161001");
        request.setLoanAccNo("LA02580533285171001");
        QueryLogLoanAccountResponse response = fudianApiService.queryLogLoanAccount(request);
        System.out.println("标的流水查询响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试账户流水查询接口
     */
    @Test
    public void testQueryLogAccount() {
        QueryLogAccountRequest request = new QueryLogAccountRequest();
        request.setAccountNo("UA02579819520851001");
        request.setUserName("UU02579819517821001");
        request.setQueryOrderDate("20180306");
        QueryLogAccountResponse response = fudianApiService.queryLogAccount(request);
        System.out.println("账户流水查询响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试提现退汇查询接口
     */
    @Test
    public void testQueryRetremit() {
        QueryRetremitRequest request = new QueryRetremitRequest();
        request.setQueryDate("20180306");
        QueryRetremitResponse response = fudianApiService.queryRetremit(request);
        System.out.println("提现退汇查询响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试数据迁移接口
     */
    @Test
    public void testMigrationData() {
        MigrationDataRequest request = new MigrationDataRequest();
        request.setFileName("dsdsds.txt");
        request.setMigrationType(1);
        request.setTotalCount(100);
        MigrationDataResponse response = fudianApiService.migrationData(request);
        System.out.println("数据迁移响应报文："+ JSON.toJSONString(response));
    }

    @Test
    public void testLoanCallback() {
        LoanCallbackRequest request = new LoanCallbackRequest();
        List<LoanCallbackInvestInfo> investInfos = Lists.newArrayList();
        for(int i=0;i<2;i++) {
            LoanCallbackInvestInfo investInfo = new LoanCallbackInvestInfo();
            investInfo.setCapital("323"+i);
            investInfo.setInterest("dsd");
            investInfo.setInterestFee("20");
            investInfos.add(investInfo);
        }
        request.setInvestList(investInfos);
        request.setLoanTxNo("23234433");
        LoanCallbackResponse response = fudianApiService.loanCallback(request);
        System.out.println("提现退汇查询响应报文："+ JSON.toJSONString(response));
    }

}
