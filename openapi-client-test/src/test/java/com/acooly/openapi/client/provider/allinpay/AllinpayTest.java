package com.acooly.openapi.client.provider.allinpay;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.allinpay.AllinpayApiService;
import com.acooly.module.openapi.client.provider.allinpay.message.AllinpayBillDownloadResponse;
import com.acooly.module.openapi.client.provider.allinpay.message.AllinpaySinglePaymentResponse;
import com.acooly.module.openapi.client.provider.allinpay.message.AllinpayTradeQueryResponse;
import com.acooly.module.openapi.client.provider.allinpay.message.dto.AllinpayBillDownloadRequestBody;
import com.acooly.module.openapi.client.provider.allinpay.message.dto.AllinpaySinglePaymentRequestBody;
import com.acooly.module.openapi.client.provider.allinpay.message.dto.AllinpayTradeQueryRequestBody;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Auther: zhike
 * @Date: 2018/8/29 15:24
 * @Description:
 */
@SpringBootApplication
@BootApp(sysName = "allinpayTest")
public class AllinpayTest extends NoWebTestBase {


    @Autowired
    private AllinpayApiService allinpayApiService;

    /**
     * 订单查询
     */
    @Test
    public void singlePayment() {
        AllinpaySinglePaymentRequestBody request = new AllinpaySinglePaymentRequestBody();
        request.setBizOrderNo(Ids.oid());
        request.setBusinessCode("09900");
        request.setAccountNo("6217001210024455227");
        request.setAccountName("张三");
        request.setAmount("100");
        AllinpaySinglePaymentResponse response = allinpayApiService.singlePayment(request);
        System.out.println("单笔代付接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 订单查询
     */
    @Test
    public void tradeQuery() {
        AllinpayTradeQueryRequestBody request = new AllinpayTradeQueryRequestBody();
        request.setQuerySn("o18091715392315920001");
        AllinpayTradeQueryResponse response = allinpayApiService.tradeQuery(request);
        System.out.println("交易查询接口响应报文："+ JSON.toJSONString(response));
    }


    /**
     * 对账文件下载
     */
    @Test
    public void billDownload() {
        AllinpayBillDownloadRequestBody request = new AllinpayBillDownloadRequestBody();
        request.setStartDay("20180829000000");
        request.setEndDay("20180829235959");
        AllinpayBillDownloadResponse response = allinpayApiService.billDownload(request);
        System.out.println("对账文件下载接口响应报文："+ JSON.toJSONString(response));
    }
}
