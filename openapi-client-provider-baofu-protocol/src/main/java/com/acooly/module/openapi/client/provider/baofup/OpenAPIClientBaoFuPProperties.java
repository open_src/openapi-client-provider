/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-16 16:11 创建
 */
package com.acooly.module.openapi.client.provider.baofup;

import com.acooly.core.utils.Strings;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.acooly.module.openapi.client.provider.baofup.OpenAPIClientBaoFuPProperties.PREFIX;

/**
 * @author zhike
 */
@ConfigurationProperties(prefix = PREFIX)
public class OpenAPIClientBaoFuPProperties {
    public static final String PREFIX = "acooly.openapi.client.baofup";

    /**
     * 网关地址
     */
    private String gatewayUrl;

    /**
     * 公钥文件路径
     */
    private String publicKeyPath;

    /**
     * 私钥文件路径
     */
    private String privateKeyPath;

    /**
     * aes对称加密密钥
     */
    private String aesKey;

    /**
     * 私钥文件密码
     */
    private String privateKeyPassword;

    /**
     * 连接超时时间（毫秒）
     */
    private long connTimeout = 10000;

    /**
     * 读超时时间（毫秒）
     */
    private long readTimeout = 30000;

    /**
     * 商户号
     */
    private String partnerId;

    /**
     * 终端号
     */
    private String terminalId;

    /**
     * 异步通知URL前缀
     */
    private String notifyUrlPrefix = "/";

    private String domainUrl;

    /**
     * 对账文件路径
     */
    private String filePath;

    protected String getCanonicalUrl(String prefix, String postfix) {
        if (Strings.endsWith(prefix, "/")) {
            prefix = Strings.removeEnd(prefix, "/");
        }
        if (Strings.startsWith(postfix, "/")) {
            return prefix + postfix;
        } else {
            return prefix + "/" + postfix;
        }
    }

    public String getGatewayUrl() {
        return gatewayUrl;
    }

    public void setGatewayUrl(String gatewayUrl) {
        this.gatewayUrl = gatewayUrl;
    }

    public String getPublicKeyPath() {
        return publicKeyPath;
    }

    public void setPublicKeyPath(String publicKeyPath) {
        this.publicKeyPath = publicKeyPath;
    }

    public String getPrivateKeyPath() {
        return privateKeyPath;
    }

    public void setPrivateKeyPath(String privateKeyPath) {
        this.privateKeyPath = privateKeyPath;
    }

    public long getConnTimeout() {
        return connTimeout;
    }

    public void setConnTimeout(long connTimeout) {
        this.connTimeout = connTimeout;
    }

    public long getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(long readTimeout) {
        this.readTimeout = readTimeout;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getNotifyUrlPrefix() {
        return notifyUrlPrefix;
    }

    public void setNotifyUrlPrefix(String notifyUrlPrefix) {
        this.notifyUrlPrefix = notifyUrlPrefix;
    }

    public String getPrivateKeyPassword() {
        return privateKeyPassword;
    }

    public void setPrivateKeyPassword(String privateKeyPassword) {
        this.privateKeyPassword = privateKeyPassword;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getDomainUrl() {
        return domainUrl;
    }

    public void setDomainUrl(String domainUrl) {
        this.domainUrl = domainUrl;
    }

    public String getAesKey() {
        return aesKey;
    }

    public void setAesKey(String aesKey) {
        this.aesKey = aesKey;
    }
}
