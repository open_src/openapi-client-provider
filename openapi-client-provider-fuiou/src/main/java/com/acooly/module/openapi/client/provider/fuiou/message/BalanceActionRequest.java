/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月5日
 *
 */
package com.acooly.module.openapi.client.provider.fuiou.message;

import com.acooly.core.utils.Dates;
import com.acooly.module.openapi.client.provider.fuiou.domain.FuiouRequest;
import com.acooly.module.openapi.client.provider.fuiou.support.FuiouAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;
import java.util.Date;

/**
 * 余额查询 请求报文
 * 
 * @author zhangpu
 */
public class BalanceActionRequest extends FuiouRequest {

	/** 交易日期 20140101 */
	@NotEmpty
	@Size(min = 8, max = 8)
	@FuiouAlias("mchnt_txn_dt")
	private String mchntTxnDt = Dates.format(new Date(), "yyyyMMdd");

	/** 用户名 */
	@Size(min = 1, max = 20)
	@FuiouAlias("cust_no")
	private String custNo;

	public BalanceActionRequest() {
	}

	/**
	 * @param custNo
	 */
	public BalanceActionRequest(String custNo) {
		super();
		this.custNo = custNo;
	}

	public String getMchntTxnDt() {
		return mchntTxnDt;
	}

	public void setMchntTxnDt(String mchntTxnDt) {
		this.mchntTxnDt = mchntTxnDt;
	}

	public String getCustNo() {
		return custNo;
	}

	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}

}
