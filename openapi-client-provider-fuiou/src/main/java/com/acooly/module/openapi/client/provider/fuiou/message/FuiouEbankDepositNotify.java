/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月5日
 *
 */
package com.acooly.module.openapi.client.provider.fuiou.message;

import com.acooly.core.utils.validate.jsr303.MobileNo;
import com.acooly.module.openapi.client.provider.fuiou.domain.FuiouNotify;
import com.acooly.module.openapi.client.provider.fuiou.support.FuiouAlias;

import javax.validation.constraints.Size;

/**
 * @author zhangpu
 */
public class FuiouEbankDepositNotify extends FuiouNotify {

	/** 手机号码 */
	@MobileNo
	@FuiouAlias("mobile_no")
	private String mobileNo;

	/** 单位：分 */
	@Size(min = 1, max = 10)
	@FuiouAlias("amt")
	private String amt;

	@Size(min = 1, max = 60)
	@FuiouAlias
	private String rem;

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getRem() {
		return rem;
	}

	public void setRem(String rem) {
		this.rem = rem;
	}

}
