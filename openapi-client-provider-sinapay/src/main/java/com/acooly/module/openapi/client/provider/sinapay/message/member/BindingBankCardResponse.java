/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月29日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.BINDING_BANK_CARD, type = ApiMessageType.Response)
public class BindingBankCardResponse extends SinapayResponse {

	/** 绑卡ID */
	@Size(max = 32)
	@NotEmpty
	@ApiItem(value = "card_id")
	private String cardId;

	/**
	 * 是否采用卡认证的方式
	 * 
	 * 如果之前请求中此卡采用的是卡认证的方式（verify_mode不为空），则返回Y，注意此参数和卡是否通过银行认证无关
	 */
	@Size(max = 1)
	@NotEmpty
	@ApiItem(value = "is_verified")
	private String isVerified;

	/** 后续推进需要的参数，如果需要推进则会返回此参数，支付推进时需要带上此参数，ticket有效期为15分钟，可以多次使用（最多5次） */
	@Size(max = 100)
	private String ticket;

}
