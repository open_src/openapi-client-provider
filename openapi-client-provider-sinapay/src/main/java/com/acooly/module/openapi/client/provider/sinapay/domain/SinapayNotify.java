/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月4日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.domain;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
@Getter
@Setter
public class SinapayNotify implements ApiMessage {

	@NotEmpty
	@ApiItem
	private String service;

	@NotEmpty
	@ApiItem("partner_id")
	private String partner;

	/**
	 * 通知类型
	 *
	 * 订单处理结果通知类型，见附录”通知业务类型”
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "notify_type")
	private String notifyType;

	/**
	 * 通知编号
	 *
	 * 32位长，随机生成
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "notify_id")
	private String notifyId;

	/**
	 * 参数编码字符集
	 *
	 * 商户网站使用的编码格式，如utf-8、gbk、gb2312等。
	 */
	@NotEmpty
	@Size(max = 10)
	@ApiItem(value = "_input_charset")
	private String inputCharset;

	/**
	 * 通知时间
	 *
	 * 通知时间，格式yyyyMMddHHmmss
	 */
	@NotEmpty
	@Size(max = 14)
	@ApiItem(value = "notify_time")
	private String notifyTime;

	/**
	 * 签名
	 *
	 * 参见“签名机制”。
	 */
	@NotEmpty
	@Size(max = 256)
	@ApiItem(value = "sign")
	private String sign;

	/**
	 * 签名方式
	 *
	 * 签名方式支持RSA、MD5。建议使用MD5
	 */
	@NotEmpty
	@Size(max = 10)
	@ApiItem(value = "sign_type", securet = true)
	private String signType;

	/**
	 * 接口版本号
	 *
	 * 接口版本号
	 */
	@NotEmpty
	@Size(max = 5)
	@ApiItem(value = "version")
	private String version;

	/**
	 * 备注
	 *
	 * 说明信息，与请求中memo内容一致
	 */
	@Size(max = 1000)
	@ApiItem(value = "memo")
	private String memo;

	/**
	 * 返回错误码
	 *
	 * 错误码，但出现业务状态为失败的情况提供错误原因，具体见附录响应码
	 */
	@Size(max = 30)
	@ApiItem(value = "error_code")
	private String errorCode;

	/**
	 * 返回错误原因
	 *
	 * 错误消息
	 */
	@Size(max = 200)
	@ApiItem(value = "error_message")
	private String errorMessage;
}
