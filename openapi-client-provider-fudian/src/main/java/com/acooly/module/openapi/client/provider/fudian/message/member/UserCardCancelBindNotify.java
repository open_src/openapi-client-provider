/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-10 03:04:05 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianNotify;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-10 03:04:05
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.USER_CARD_CANCELBIND ,type = ApiMessageType.Notify)
public class UserCardCancelBindNotify extends FudianNotify {

    /**
     * 账户号
     * 用户在本系统的唯一账户编号，由本系统生成.
     */
    @NotEmpty
    @Length(max=50)
    private String accountNo;

    /**
     * 解绑银行
     * 解绑接口开户银行
     */
    @NotEmpty
    @Length(max=256)
    private String bank;

    /**
     * 解绑银行卡号
     * 解绑的银行卡号
     */
    @NotEmpty
    @Length(max=32)
    private String bankAccountNo;

    /**
     * 银行对应编码
     * 解绑银行对应的编码，见附录【4.4】
     */
    @NotEmpty
    @Length(max=32)
    private String bankCode;

    /**
     * 绑卡状态
     * 1-解绑失败， 4-解绑成功
     */
    @NotEmpty
    @Length(max=2)
    private String status;

    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    @NotEmpty
    @Length(max=8)
    private String merchantNo;

    /**
     * 用户名
     * 用户在本系统的唯一标识，由本系统生成
     */
    @NotEmpty
    @Length(max=32)
    private String userName;
}