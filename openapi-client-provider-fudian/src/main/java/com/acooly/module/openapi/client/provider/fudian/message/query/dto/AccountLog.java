package com.acooly.module.openapi.client.provider.fudian.message.query.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class AccountLog {

    /**
     * 操作金额
     * 操作金额
     */
    @NotEmpty
    @Length(max = 20)
    private String amount;

    /**
     * 可用余额
     * 可用余额
     */
    @NotEmpty
    @Length(max = 20)
    private String balance;

    /**
     * 发生时间
     * 添加记录时间
     */
    @NotEmpty
    private String createTime;

    /**
     * 冻结金额
     * 冻结金额
     */
    @NotEmpty
    @Length(max = 20)
    private String freezeBalance;

    /**
     * 商户名称
     * 商户名称
     */
    @NotEmpty
    @Length(max = 32)
    private String merchantName;

    /**
     * 托管用户id
     * 商户号
     */
    @NotEmpty
    @Length(max = 32)
    private String merchantNo;

    /**
     * 交易类型
     * 交易说明信息
     */
    @NotEmpty
    @Length(min = 1, max = 1)
    private String remark;

    /**
     * 存管用户用户名
     * 交易对方在存管系统交易用户名
     */
    @NotEmpty
    private String toUserName;

    /**
     * 托管账户用户名
     * 托管账户用户名
     */
    @NotEmpty
    @Length(max = 32)
    private String userName;
}
