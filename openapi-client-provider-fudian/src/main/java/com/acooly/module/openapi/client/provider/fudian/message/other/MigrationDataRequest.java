/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 01:54:36 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.other;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianRequest;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * @author zhangpu 2018-02-22 01:54:36
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.MIGRATION_DATA ,type = ApiMessageType.Request)
public class MigrationDataRequest extends FudianRequest {

    /**
     * 迁移类型
     * 1-用户，2-资金，3-投资，4-标的
     */
    @NotNull
    private int migrationType = 1;

    /**
     * 文件名称
     * ${merchant_no}
     */
    @NotEmpty
    @Length(max=256)
    private String fileName;

    /**
     * 总条数
     * 文件内参数总条数
     */
    @NotNull
    private int totalCount = 100;
}