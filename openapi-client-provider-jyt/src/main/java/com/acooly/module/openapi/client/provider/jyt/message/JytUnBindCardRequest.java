package com.acooly.module.openapi.client.provider.jyt.message;

import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.jyt.domain.JytApiMsgInfo;
import com.acooly.module.openapi.client.provider.jyt.domain.JytRequest;
import com.acooly.module.openapi.client.provider.jyt.enums.JytServiceEnum;
import com.acooly.module.openapi.client.provider.jyt.message.dto.JytUnBindCardRequestBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/6/19 22:05
 */
@Getter
@Setter
@JytApiMsgInfo(service = JytServiceEnum.UN_BIND_CARD,type = ApiMessageType.Request)
@XStreamAlias("message")
public class JytUnBindCardRequest extends JytRequest {
    /**
     * 请求报文体
     */
    @XStreamAlias("body")
    private JytUnBindCardRequestBody unBindCardRequestBody;

    @Override
    public void doCheck() {
        Validators.assertJSR303(getHeaderRequest());
        Validators.assertJSR303(getUnBindCardRequestBody());
    }
}
