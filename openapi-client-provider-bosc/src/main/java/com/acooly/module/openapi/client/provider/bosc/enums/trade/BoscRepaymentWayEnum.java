/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 19:29 创建
 */
package com.acooly.module.openapi.client.provider.bosc.enums.trade;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 还款方式
 *
 * @author zhangpu 2017-09-24 19:29
 */
public enum BoscRepaymentWayEnum implements Messageable {
 
    ONE_TIME_SERVICING  ("ONE_TIME_SERVICING", "一次性还本付息"),
    FIRSEINTREST_LASTPRICIPAL  ("FIRSEINTREST_LASTPRICIPAL", "先息后本"),
    FIXED_PAYMENT_MORTGAGE  ("FIXED_PAYMENT_MORTGAGE", "等额本息"),
    FIXED_BASIS_MORTGAGE  ("FIXED_BASIS_MORTGAGE", "等额本金"),
    ;

    private final String code;
    private final String message;

    private BoscRepaymentWayEnum (String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (BoscRepaymentWayEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static BoscRepaymentWayEnum find(String code) {
        for (BoscRepaymentWayEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("BoscServiceNameEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<BoscRepaymentWayEnum> getAll() {
        List<BoscRepaymentWayEnum> list = new ArrayList<BoscRepaymentWayEnum>();
        for (BoscRepaymentWayEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (BoscRepaymentWayEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
