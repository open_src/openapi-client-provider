/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-25 15:53 创建
 */
package com.acooly.module.openapi.client.provider.bosc.enums.fund;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 银行编号
 * @author zhangpu 2017-09-25 15:53
 */
public enum BoscBankcodeEnum implements Messageable {
    
    ABOC("ABOC", "农业银行"),
    
    FJIB("FJIB","兴业银行"),
    MSBC("MSBC","民生银行"),
    SPDB("SPDB","浦发银行"),
    BKCH("BKCH", "中国银行"),
    
    GDBK("GDBK","广发银行"),
    PCBC("PCBC","建设银行"),
    BJCN("BJCN","北京银行"),
    CIBK("CIBK","中信银行"),
    
    HXBK("HXBK","华夏银行"),
    PSBC("PSBC","中国邮储"),
    BOSH("BOSH","上海银行"),
    EVER("EVER","光大银行"),
    
    ICBK("ICBK","工商银行"),
    SZDB("SZDB","平安银行"),
    COMM("COMM","交通银行"),
    CMBC("CMBC","招商银行"),
    ;


    private final String code;
    private final String message;

    private BoscBankcodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (BoscBankcodeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static BoscBankcodeEnum find(String code) {
        for (BoscBankcodeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("BoscBankcodeEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<BoscBankcodeEnum> getAll() {
        List<BoscBankcodeEnum> list = new ArrayList<BoscBankcodeEnum>();
        for (BoscBankcodeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (BoscBankcodeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
