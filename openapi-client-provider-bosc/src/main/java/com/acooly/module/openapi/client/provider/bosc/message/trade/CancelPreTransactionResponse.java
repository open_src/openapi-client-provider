package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;

@ApiMsgInfo(service = BoscServiceNameEnum.CANCEL_PRE_TRANSACTION, type = ApiMessageType.Response)
public class CancelPreTransactionResponse extends BoscResponse {

}