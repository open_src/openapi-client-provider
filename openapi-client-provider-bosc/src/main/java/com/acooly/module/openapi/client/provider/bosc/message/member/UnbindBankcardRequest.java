package com.acooly.module.openapi.client.provider.bosc.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * Created by liubin@prosysoft.com on 2017/10/10.
 */
@ApiMsgInfo(service = BoscServiceNameEnum.UNBIND_BANKCARD, type = ApiMessageType.Request)
public class UnbindBankcardRequest extends BoscRequest {
	
	/**
	 * 平台用户编号，
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	
	/**
	 * 页面回跳 URL
	 */
	@NotEmpty
	@Size(max = 100)
	private String redirectUrl;
	
	public UnbindBankcardRequest () {
		setService (BoscServiceNameEnum.UNBIND_BANKCARD.code ());
	}
	
	public UnbindBankcardRequest (String platformUserNo, String requestNo, String redirectUrl) {
		this();
		this.platformUserNo = platformUserNo;
		this.requestNo = requestNo;
		this.redirectUrl = redirectUrl;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getRedirectUrl () {
		return redirectUrl;
	}
	
	public void setRedirectUrl (String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
}
