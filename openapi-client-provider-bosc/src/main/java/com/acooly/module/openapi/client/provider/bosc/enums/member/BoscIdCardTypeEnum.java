/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-25 15:34 创建
 */
package com.acooly.module.openapi.client.provider.bosc.enums.member;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 证件类型
 *
 * @author zhangpu 2017-09-25 15:34
 */
public enum BoscIdCardTypeEnum implements Messageable {


    PRC_ID("PRC_ID", "身份证"),

    PASSPORT("PASSPORT", "护照"),

    COMPATRIOTS_CARD("COMPATRIOTS_CARD", "港澳台同胞回乡证"),

    PERMANENT_RESIDENCE("PERMANENT_RESIDENCE", "外国人永久居留证");


    private final String code;
    private final String message;

    private BoscIdCardTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (BoscIdCardTypeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static BoscIdCardTypeEnum find(String code) {
        for (BoscIdCardTypeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("BoscIdCardTypeEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<BoscIdCardTypeEnum> getAll() {
        List<BoscIdCardTypeEnum> list = new ArrayList<BoscIdCardTypeEnum>();
        for (BoscIdCardTypeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (BoscIdCardTypeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
