/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-16 16:11 创建
 */
package com.acooly.module.openapi.client.provider.yibao;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.acooly.module.openapi.client.provider.yibao.OpenAPIClientYibaoProperties.PREFIX;


/**
 * 翼支付配置参数
 *
 * @author zhangpu@acooly.cn
 */
@Getter
@Setter
@ConfigurationProperties(prefix = PREFIX)
public class OpenAPIClientYibaoProperties {

    public static final String PREFIX = "acooly.openapi.client.yibao";

    /**
     * 网关地址
     */
    private String gatewayUrl;

    /**
     * 查询接口地址
     */
    private String queryGatewayUrl;


    /**
     * 下载网关地址
     */
    private String downloadUrl;

    /**
     * 商户号
     */
    private String merchantNo;


    /**
     * 商户私钥
     */
    private String privateKey;

    /**
     * 网关公钥
     */
    private String publicKey;

    /**
     * AES密钥
     */
    private String aesKey;

    /**
     * 本系统域名（用于自动生成回调地址）
     */
    private String domain;

    /**
     * 本系统的通知Url或URL前缀
     */
    private String notifyUrl = "/openapi/client/shengpay/notify";

    /**
     * 连接超时时间（毫秒）
     */
    private long connTimeout = 10000;

    /**
     * 读超时时间（毫秒）
     */
    private long readTimeout = 30000;


    private Checkfile checkfile = new Checkfile();

    @Getter
    @Setter
    public static class Checkfile {

        private String host;
        private int port;
        private String username;
        private String password;

        private String serverRoot;
        private String localRoot;

    }


}
