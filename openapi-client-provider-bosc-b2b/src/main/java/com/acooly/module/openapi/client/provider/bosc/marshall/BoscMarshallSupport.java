package com.acooly.module.openapi.client.provider.bosc.marshall;

import org.springframework.beans.factory.annotation.Autowired;

import com.acooly.module.openapi.client.provider.bosc.BoscProperties;
import com.acooly.module.safety.signature.SignerFactory;
import com.acooly.module.safety.support.KeyStoreInfo;

public class BoscMarshallSupport {

	@Autowired
	private BoscProperties properties;

	@SuppressWarnings("rawtypes")
	@Autowired
	protected SignerFactory signerFactory;

	private volatile KeyStoreInfo keyStoreInfo;

	public KeyStoreInfo getKeyStoreInfo() {
		if (null == keyStoreInfo) {
			synchronized (BoscMarshallSupport.class) {
				if (keyStoreInfo == null) {
					keyStoreInfo = new KeyStoreInfo();
					keyStoreInfo.setKeyStoreUri(properties.getCertpfx());
					keyStoreInfo.setKeyStorePassword(properties.getCertpfxPwd());
					// changed by zhangpu 2017-11-13
					// ? 不设置对方证书 ?
					keyStoreInfo.setCertificateUri(null);
					keyStoreInfo.setKeyStoreType(KeyStoreInfo.KEY_STORE_PKCS12);
					// ? 待签明文字符集 ? keyStoreInfo.setPlainEncode("utf-8");
					// ? 签名的算法 ? keyStoreInfo.setSignatureAlgo(RSA.SIGN_ALGO_SHA1);
					// ? 签名结果怎么编码？ keyStoreInfo.setSignatureCodec(CodecEnum.BASE64 or
					// CodecEnum.HEX);

					// 最后load下，内部会缓存。
					keyStoreInfo.loadKeys();
				}
			}

		}
		return keyStoreInfo;
	}

	@SuppressWarnings("rawtypes")
	public SignerFactory getSignerFactory() {
		return signerFactory;
	}

}
