/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.newyl.domain;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.thoughtworks.xstream.io.naming.NoNameCoder;
import com.thoughtworks.xstream.io.xml.DomDriver;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhangpu
 */
@Getter
@Setter
public class NewYlRequest extends NewYlApiMessage {

    @XStreamOmitField
    private XStream xStream;

    /**
     * 数据类型 xml/json
     *
     * @param
     */
    public NewYlRequest() {
        xStream = new XStream(new DomDriver("GBK", new NoNameCoder()));
        // 启用Annotation
        xStream.autodetectAnnotations(true);
    }

    public String obj2Str(Object obj) {
        return xStream.toXML(obj);
    }



}
