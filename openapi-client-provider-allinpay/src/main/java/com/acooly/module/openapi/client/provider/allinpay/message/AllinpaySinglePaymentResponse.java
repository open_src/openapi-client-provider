package com.acooly.module.openapi.client.provider.allinpay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayApiMsgInfo;
import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayResponse;
import com.acooly.module.openapi.client.provider.allinpay.enums.AllinpayServiceEnum;
import com.acooly.module.openapi.client.provider.allinpay.message.dto.AllinpaySinglePaymentResponseBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @Auther: zhike
 * @Date: 2018/8/29 19:10
 * @Description:
 */
@Getter
@Setter
@AllinpayApiMsgInfo(service = AllinpayServiceEnum.SINGLE_PAYMENT,type = ApiMessageType.Response)
@XStreamAlias("AIPG")
public class AllinpaySinglePaymentResponse extends AllinpayResponse {

    /**
     * 响应报文体
     */
    @XStreamAlias("TRANSRET")
    private AllinpaySinglePaymentResponseBody responseBody;
}
