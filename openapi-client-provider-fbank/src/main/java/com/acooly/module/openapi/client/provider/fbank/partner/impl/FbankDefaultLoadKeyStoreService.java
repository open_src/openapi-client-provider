//package com.acooly.module.openapi.client.provider.fbank.partner.impl;
//
//import com.acooly.core.utils.Strings;
//import com.acooly.module.openapi.client.provider.fbank.OpenAPIClientFbankProperties;
//import com.acooly.module.openapi.client.provider.fbank.FbankConstants;
//import com.acooly.module.safety.key.AbstractKeyLoadManager;
//import com.acooly.module.safety.key.KeySimpleLoader;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//@Component
//public class FbankDefaultLoadKeyStoreService extends AbstractKeyLoadManager<String> implements KeySimpleLoader {
//
//    @Autowired
//    private OpenAPIClientFbankProperties openAPIClientFbankProperties;
//
//    @Override
//    public String doLoad(String principal) {
//        if (Strings.equals("7551000001", principal)) {
//            return openAPIClientFbankProperties.getPrivateKey();
//        } else {
//            //微信APP支付的时候使用
//            return "7daa4babae15ae17eee90c9e";
//        }
//    }
//
//    @Override
//    public String getProvider() {
//        return FbankConstants.PROVIDER_NAME;
//    }
//}
