package com.acooly.module.openapi.client.provider.fbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankResponse;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike@qq.com
 * @date 2018-09-06 11:48
 */
@Slf4j
@Getter
@Setter
@FbankApiMsgInfo(service = FbankServiceEnum.TRADE_DOWNLOAD_FILE, type = ApiMessageType.Response)
public class FbankReconFileUrlQueryResponse extends FbankResponse {


    /**
     * 对账文件下载路径
     */
    private String filePath;

    /**
     * 商户编号
     */
    private String mchntId;
}
