package com.acooly.module.openapi.client.provider.fbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankResponse;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike@acooly.com
 * @date 2018-09-07 17:38
 */
@Getter
@Setter
@FbankApiMsgInfo(service = FbankServiceEnum.TRADE_CLOSE, type = ApiMessageType.Response,isVerifySign = false)
public class FbankTradeCloseResponse extends FbankResponse {

    /**
     * 订单关单状态
     * success:撤销成功; fail:撤销失败
     */
    @NotBlank
    @Size(max = 12)
    private String closeStatus;

    /**
     * 错误消息
     * 关单失败原因
     */
    @Size(max = 16)
    private String errorMsg;
}
