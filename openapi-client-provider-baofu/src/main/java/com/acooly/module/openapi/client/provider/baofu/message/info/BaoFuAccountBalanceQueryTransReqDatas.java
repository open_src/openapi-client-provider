package com.acooly.module.openapi.client.provider.baofu.message.info;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhike 2018/3/1 18:16
 */
@Getter
@Setter
@XStreamAlias("trans_reqDatas")
public class BaoFuAccountBalanceQueryTransReqDatas implements Serializable{

    @XStreamImplicit(itemFieldName="trans_reqData")//标注加在list上
    private List<BaoFuAccountBalanceQueryTransReqData> transReqDatas;

}
