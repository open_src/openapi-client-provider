package com.acooly.module.openapi.client.provider.baofu.message.info;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author zhike 2018/2/2 11:53
 */
@Getter
@Setter
@XStreamAlias("trans_reqData")
public class BaoFuWithdrawHttpRequestInfo implements Serializable{
    /**
     * 商户订单号
     * 循环域
     * 商户唯一流水号，
     * 具体格式建议：商户编号+14位日期+3位随机数
     */
    @XStreamAlias("trans_no")
    private String transNo;

    /**
     *
     */
    @XStreamAlias("trans_money")
    private String transMoney;// 转账金额

    /**
     *
     */
    @XStreamAlias("to_acc_name")
    private String toAccName;// 收款人姓名

    /**
     *
     */
    @XStreamAlias("trans_mobile")
    private String transMobile;//银行卡预留手机号

    /**
     *
     */
    @XStreamAlias("trans_card_id")
    private String transCardId;//银行卡身份证件号码

    /**
     *
     */
    @XStreamAlias("to_acc_no")
    private String toAccNo;// 收款人银行帐号

    /**
     *
     */
    @XStreamAlias("to_bank_name")
    private String toBankName;// 收款人银行名称

    /**
     *
     */
    @XStreamAlias("to_pro_name")
    private String toProName;// 收款人开户行省名

    /**
     *
     */
    @XStreamAlias("to_city_name")
    private String toCityName;// 收款人开户行市名

    /**
     *
     */
    @XStreamAlias("to_acc_dept")
    private String toAccDept;// 收款人开户行机构名

    /**
     *
     */
    @XStreamAlias("trans_summary")
    private String transSummary;//摘要
}
