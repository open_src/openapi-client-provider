package com.acooly.module.openapi.client.provider.fuyou;

import com.google.common.collect.Lists;

import com.acooly.module.openapi.client.api.notify.ApiServiceClientServlet;
import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.acooly.module.openapi.client.provider.fuyou.notify.FuyouApiServiceClientServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;

import static com.acooly.module.openapi.client.provider.fuyou.OpenAPIClientFuyouProperties.PREFIX;

@EnableConfigurationProperties({OpenAPIClientFuyouProperties.class})
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class OpenAPIClientFuyouConfigration {

    @Autowired
    private OpenAPIClientFuyouProperties openAPIClientFuiouProperties;

    @Bean("fuyouHttpTransport")
    public Transport fuyouHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setCharset("UTF-8");
        httpTransport.setContentType("application/x-www-form-urlencoded");
        httpTransport.setConnTimeout(String.valueOf(openAPIClientFuiouProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(openAPIClientFuiouProperties.getReadTimeout()));
        return httpTransport;
    }

    /**
     * 富友SDK-servlet注册
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean getFuyouApiSDKServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean();
        FuyouApiServiceClientServlet apiServiceClientServlet = new FuyouApiServiceClientServlet();
        bean.setServlet(apiServiceClientServlet);
        bean.addInitParameter(ApiServiceClientServlet.NOTIFY_DISPATCHER_BEAN_NAME_KEY, "fuyouNotifyHandlerDispatcher");
        List<String> urlMappings = Lists.newArrayList();       //网关异步通知地址
        urlMappings.add("/gateway/notify/fuyouNotify/" + FuyouServiceEnum.FIRST_AGREEMENT_PAY.getCode());
        urlMappings.add("/gateway/notify/fuyouNotify/" + FuyouServiceEnum.AGREEMENT_PAY.getCode());//访问，可以添加多个
        urlMappings.add("/gateway/notify/fuyouNotify/" + FuyouServiceEnum.FUYOU_NETBANK.getCode());//访问，可以添加多个
        urlMappings.add("/gateway/notify/fuyouNotify/" + FuyouServiceEnum.FUYOU_WITHDRAW.getCode());//访问，可以添加多个
        urlMappings.add("/gateway/notify/fuyouNotify/" + FuyouServiceEnum.FUYOU_ORDER_PAY.getCode());//访问，可以添加多个
        bean.setUrlMappings(urlMappings);
        bean.setLoadOnStartup(3);
        return bean;
    }

}
