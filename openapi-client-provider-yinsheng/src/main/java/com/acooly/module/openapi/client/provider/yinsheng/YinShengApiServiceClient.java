/**
 * create by zhangpu
 * date:2015年3月2日
 */
package com.acooly.module.openapi.client.provider.yinsheng;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.AbstractApiServiceClient;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.exception.ApiServerException;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengNotify;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengRequest;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengResponse;
import com.acooly.module.openapi.client.provider.yinsheng.marshall.*;
import com.acooly.module.openapi.client.provider.yinsheng.utils.StringHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * ApiService执行器
 *
 * @author zhangpu
 */
@Component("yinShengApiServiceClient")
@Slf4j
public class YinShengApiServiceClient
        extends AbstractApiServiceClient<YinShengRequest, YinShengResponse, YinShengNotify, YinShengNotify> {


    public static final String PROVIDER_NAME = "yinsheng";

    @Resource(name = "yinShengHttpTransport")
    private Transport transport;

    @Resource(name = "yinShengRequestMarshall")
    private YinShengRequestMarshall requestMarshal;

    @Resource(name = "yinShengRedirectMarshall")
    private YinShengRedirectMarshall redirectMarshal;

    @Resource(name = "yinShengSignMarshall")
    private YinShengSignMarshall signMarshall;

    @Resource(name = "yinShengRedirectPostMarshall")
    private YinShengRedirectPostMarshall yinShengRedirectPostMarshall;

    @Autowired
    private YinShengResponseUnmarshall responseUnmarshal;
    @Autowired
    private YinShengNotifyUnmarshall notifyUnmarshal;

    @Autowired
    private OpenAPIClientYinShengProperties openAPIClientYinShengProperties;

    @Override
    public YinShengResponse execute(YinShengRequest request) {
        try {
            beforeExecute(request);
            if(Strings.isBlank(request.getGatewayUrl())) {
                request.setGatewayUrl(openAPIClientYinShengProperties.getGatewayUrl());
            }
            String url = request.getGatewayUrl();
            String requestMessage = getRequestMarshal().marshal(request);
            log.info("请求报文: {}", requestMessage);
            String responseMessage = getTransport().exchange(requestMessage, url);
            if(Strings.isBlank(responseMessage)) {
                throw new ApiClientException("响应报文位空");
            }
            YinShengResponse t = getResponseUnmarshal().unmarshal(responseMessage, request.getService());
            afterExecute(t);
            return t;
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    @Override
    public String redirectGet(YinShengRequest request) {
        PostRedirect postRedirect = redirectPost(request);
        String fromHtml = StringHelper.netbankBuildFormHtml(postRedirect.getRedirectUrl(), postRedirect.getFormDatas(), YinShengConstants.NETBANK_NAME);
        return fromHtml;
    }

    public YinShengNotify notice(HttpServletRequest request, String serviceKey) {
        try {
            Map<String,String> notifyData = getSignMarshall().getDateMap(request);
            YinShengNotify notify = getNoticeUnmarshal().unmarshal(notifyData, serviceKey);
            afterNotice(notify);
            return notify;
        } catch (ApiClientException oce) {
            log.warn("客户端:{}", oce.getMessage());
            throw oce;
        } catch (Exception e) {
            log.warn("内部错误:{}", e.getMessage());
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    @Override
    public PostRedirect redirectPost(YinShengRequest request) {
        try {
            beforeExecute(request);
            PostRedirect postRedirect = yinShengRedirectPostMarshall.marshal(request);
            return postRedirect;
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    @Override
    protected ApiMarshal<String, YinShengRequest> getRequestMarshal() {
        return this.requestMarshal;
    }

    @Override
    protected ApiUnmarshal<YinShengResponse, String> getResponseUnmarshal() {
        return this.responseUnmarshal;
    }

    @Override
    protected ApiUnmarshal<YinShengNotify, Map<String, String>> getNoticeUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    protected ApiMarshal<String, YinShengRequest> getRedirectMarshal() {
        return this.redirectMarshal;
    }

    @Override
    protected Transport getTransport() {
        return this.transport;
    }

    @Override
    protected ApiUnmarshal<YinShengNotify, Map<String, String>> getReturnUnmarshal() {
        return this.notifyUnmarshal;
    }

    public YinShengSignMarshall getSignMarshall() {
        return signMarshall;
    }

    public void setSignMarshall(YinShengSignMarshall signMarshall) {
        this.signMarshall = signMarshall;
    }

    @Override
    public String getName() {
        return PROVIDER_NAME;
    }
}
