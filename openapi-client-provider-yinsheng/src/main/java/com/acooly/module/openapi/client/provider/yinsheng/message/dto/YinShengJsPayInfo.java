package com.acooly.module.openapi.client.provider.yinsheng.message.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhike 2018/1/8 19:19
 */
@Getter
@Setter
public class YinShengJsPayInfo extends YinShengInfo {

    /**
     * 商家公众号
     * 微信用户所关注商家公众号的 openid
     */
    @NotBlank
    private String sub_openid;
}
