/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.yinsheng.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike
 * Date: 2017-03-30 15:12:06
 */
public enum YinShengServiceEnum implements Messageable {

    NETBANK_DEPOSIT("ysepay.online.directpay.createbyuser","yinShengNetBank", "网关充值"),
    TRADE_ORDER_QUEERY("ysepay.online.trade.query","yinShengTradeOrderQuery", "订单查询"),
    FAST_PAY("ysepay.online.fastpay","yinShengFastPay", "快捷申请"),
    FAST_PAY_AUTHORIZE("ysepay.online.fastpay.authorize","yinShengFastPayAuthorize", "快捷认证"),
    BILL_DOWNLOAD("ysepay.online.bill.downloadurl.get","yinShengBillDownLoad", "对账文件下载"),
    SCAN_PAY("ysepay.online.qrcodepay","yinShengScanPay", "微信支付宝扫码支付"),
    WECHAT_JSPAY("ysepay.online.jsapi.pay","yinShengWechatJsPay", "微信公众号支付"),
    APP_PAY("ysepay.online.sdkpay","yinShengAppPay", "微信支付宝app支付"),
    WITHDRAW_D0("ysepay.merchant.withdraw.d0.accept","yinShengWithdrawD0", "D0提现清算接口"),
    MERCHANT_BALANCE_QUERY("ysepay.merchant.balance.query","yinShengMerchantBalanceQuery", "商户余额查询"),
    ;

    private final String code;
    private final String key;
    private final String message;

    private YinShengServiceEnum(String code,String key, String message) {
        this.code = code;
        this.message = message;
        this.key = key;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public String getKey() {
        return key;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (YinShengServiceEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static YinShengServiceEnum find(String code) {
        for (YinShengServiceEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param key 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 key 没有对应的 Status 。
     */
    public static YinShengServiceEnum findByKey(String key) {
        for (YinShengServiceEnum status : values()) {
            if (status.getKey().equals(key)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<YinShengServiceEnum> getAll() {
        List<YinShengServiceEnum> list = new ArrayList<YinShengServiceEnum>();
        for (YinShengServiceEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (YinShengServiceEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
